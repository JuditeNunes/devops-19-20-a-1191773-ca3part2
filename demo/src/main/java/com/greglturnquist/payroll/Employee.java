/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String email;


    private Employee() {
    }

    public Employee(String firstName, String lastName, String description, String funtion, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        jobTitle = funtion;
        checkEmail(email);
        this.email=email;
       }
    /**
     * Método que devolve excepção caso email seja inválido
     * @param email - string, corresponde ao endereço eletronico do empregado a registar
     */
    private void checkEmail(String email) {
        if (!isValid(email)) throw new IllegalArgumentException("Could Not Register Employee - Email Not Valid");
    }

    /**
     *
     * @param email - string a validar para pârametros de emai
     * @return - verdadeiro se não for nula, e se não tiver carateres inválidos
     */

    public static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobTitle, employee.jobTitle) &&
                Objects.equals(email, employee.email);
                }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description, jobTitle, email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setJobTitle(String function) {
        jobTitle = function;
    }

    public String getJobTitle() {
        if (jobTitle == null) return "Job title not defined";
        String jobTit = jobTitle;
        return jobTit;
    }

    public void setEmail (String newEmail){
        email=newEmail;
    }

    public String getEmail(){
        if (email==null) return "No available email";
        String m = email;
        return m;
    }



    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", job title='" + jobTitle + '\'' +
                '}';
    }
}
// end::code[]
