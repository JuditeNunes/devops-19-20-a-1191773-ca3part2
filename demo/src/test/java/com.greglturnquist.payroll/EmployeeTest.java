package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Determinar função do trabalhador - Equals")
    void getJobTitle_1() {
        //Arrange
        Employee jose = new Employee("jose", "silva",
                "ajuda nas reposições", "auxiliar", "jose@email.pt");
        //Act
        String result = jose.getJobTitle();
        //Assert
        assertEquals("auxiliar", result);
    }

    @Test
    @DisplayName("Determinar função do trabalhador - NotEquals")
    void getJobTitle_2() {
        //Arrange
        Employee jose = new Employee("jose", "silva",
                "ajuda nas reposições", "auxiliar", "jose@email.pt");
        //Act
        String result = jose.getJobTitle();
        //Assert
        assertNotEquals("chefe", result);
    }

    @Test
    @DisplayName("Determinar função do trabalhador - Null")
    void getJobTitle_3null() {
        //Arrange
        Employee jose = new Employee("jose", "silva",
                "ajuda nas reposições", null, "jose@email.pt");
        //Act
        String result = jose.getJobTitle();
        //Assert
        assertEquals("Job title not defined", result);
    }

    @Test
    @DisplayName("Atribuir e Determinar função do trabalhador")
    void setJobTitle_1() {
        //Arrange
        Employee jose = new Employee("jose", "silva",
                "ajuda nas reposições", null, "jose@email.pt");
        //Act
        jose.setJobTitle("auxiliar");
        String result = jose.getJobTitle();
        //Assert
        assertEquals("auxiliar", result);
    }

    @Test
    @DisplayName("Atribuir e Determinar função do trabalhador")
    void setJobTitle_2() {
        //Arrange
        Employee jose = new Employee("jose", "silva",
                "ajuda nas reposições", "auxiliar", "jose@email.pt");
        //Act
        jose.setJobTitle(null);
        String result = jose.getJobTitle();
        //Assert
        assertEquals("Job title not defined", result);
    }

    @Test
    void getEmail() {
        //Arrange
        Employee maria = new Employee("maria", "pontes",
                "responsável caixa", "gerente", "maria@email.pt");
        //Act
        String result = maria.getEmail();
        //Assert
        assertEquals("maria@email.pt", result);
    }


    @Test
    void set_getEmail() {
        //Arrange
        Employee maria = new Employee("maria", "pontes",
                "responsável caixa", "gerente", "maria@email.pt");
        //Act
        maria.setEmail("mariapontes@email.pt");
        String result = maria.getEmail();
        //Assert
        assertEquals("mariapontes@email.pt", result);
    }

    @Test
    @DisplayName("Employee registered with email, but email was after deleted")
    //there is no method to prevend errase email - taks not asked to do
    void set_getEmail_Null() {
        //Arrange
        Employee maria = new Employee("maria", "pontes",
                "responsável caixa", "gerente", "maria@email.pt");
        //Act
        maria.setEmail(null);
        String result = maria.getEmail();
        //Assert
        assertEquals("No available email", result);
    }

    @Test
    void equals_True_Same() {
        //Arrange
        Employee ana = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        //Act
        boolean result = ana.equals(ana);
        //Assert
        assertTrue(result);
    }

    @Test
    void equals_True_DifName() {
        //Arrange
        Employee ana = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        Employee anaOutra = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        //Act
        boolean result = ana.equals(anaOutra);
        //Assert
        assertTrue(result);
    }

    @Test
    void equals_False_OutroEmail() {
        //Arrange
        Employee ana = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        Employee anaOutra = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaOutra@mail.com");
        //Act
        boolean result = ana.equals(anaOutra);
        //Assert
        assertFalse(result);
    }

    @Test
    void equals_False_null() {
        //Arrange
        Employee ana = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");

        //Act
        boolean result = ana.equals(null);
        //Assert
        assertFalse(result);
    }

    @Test
    void equals_False_AnotherConstrutor() {
        //Arrange
        Employee ana = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        LocalDate anaOutra = LocalDate.of(1999, 2, 4);
        //Act
        boolean result = ana.equals(anaOutra);
        //Assert
        assertFalse(result);
    }

    @Test
    void hashcode_True() {
        //Arrange
        Employee anaA = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        Employee anaB = new Employee("Ana", "Ribeiro",
                "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
        //Act
        int a = anaA.hashCode();
        int b = anaB.hashCode();
        //Assert
        assertEquals(a, b);
    }
        @Test
        void hashcode_False() {
            //Arrange
            Employee anaA = new Employee("Ana", "Ribeiro",
                    "Chefe de Loja", "Responsável", "anaribeiro@mail.com");
            Employee anaB = new Employee("Ana", "Ribeiro",
                    "Chefe de Loja", "Responsável", "anaBribeiro@mail.com");
            //Act
            int a = anaA.hashCode();
            int b = anaB.hashCode();
            //Assert
            assertNotEquals(a, b);
        }
    @Test
    void checkEmail_Null() {
        //Arrange, act
        Throwable except = assertThrows(IllegalArgumentException.class, ()-> new Employee("Ana",
                "Ribeiro","Chefe de Loja", "Responsável", null));
        //Assert
        assertEquals("Could Not Register Employee - Email Not Valid", except.getMessage());
    }

    @Test
    void checkEmail_Invalid1() {
        //Arrange, act
        Throwable except = assertThrows(IllegalArgumentException.class, ()-> new Employee("Ana",
                "Ribeiro","Chefe de Loja", "Responsável", "www.huuj.pu"));
        //Assert
        assertEquals("Could Not Register Employee - Email Not Valid", except.getMessage());
    }
    @Test
    void checkEmail_Invalid2() {
        //Arrange, act
        Throwable except = assertThrows(IllegalArgumentException.class, ()-> new Employee("Ana",
                "Ribeiro","Chefe de Loja", "Responsável", "ana***@er"));
        //Assert
        assertEquals("Could Not Register Employee - Email Not Valid", except.getMessage());
    }
    @Test
    void checkEmail_Invalid3() {
        //Arrange, act
        Throwable except = assertThrows(IllegalArgumentException.class, ()-> new Employee("Ana",
                "Ribeiro","Chefe de Loja", "Responsável", "ana@@@@pt.pt"));
        //Assert
        assertEquals("Could Not Register Employee - Email Not Valid", except.getMessage());
    }
    @Test
    void checkEmail_Invalid4() {
        //Arrange, act
        Throwable except = assertThrows(IllegalArgumentException.class, ()-> new Employee("Ana",
                "Ribeiro","Chefe de Loja", "Responsável", "ana"));
        //Assert
        assertEquals("Could Not Register Employee - Email Not Valid", except.getMessage());
    }
}